<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pharmacie extends Model
{
    protected $filable = [
        'name',
        'adress',
        'latitude',
        'logitude',
        'garde',
    ];
}
