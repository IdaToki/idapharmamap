<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitByPharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_by_pharmacies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pharmacie_id')->unsigned();
            $table->integer('produit_id')->unsigned();
            $table->timestamps();

            $table->foreign('pharmacie_id')->references('id')->on('pharmacies');
            $table->foreign('produit_id')->references('id')->on('produits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produit_by_pharmacies');
    }
}
