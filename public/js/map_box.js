/*

<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
<link href='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css' rel='stylesheet' />
<script src='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js'></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/turf/v3.0.11/turf.min.js'></script>

*/
/*var map = L.map('map', {
    center: [6.125, 1.224],
    zoom: 15
});

 L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=AWTYfcimaGJAdWe9rrEi', {
     attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
 }).addTo(map);

 //var marker = L.marker([6.125, 1.224]).addTo(map);*/

  /*mapboxgl.accessToken = 'pk.eyJ1Ijoib2ZmLWluaSIsImEiOiJjazdpMzdwenowZm9sM2ZvM3Y4NTJvbzIxIn0.eVWPpO-Cud_gJDpUetzB7Q';
 var map = new mapboxgl.Map({
 container: 'map',
 style: 'mapbox://styles/mapbox/streets-v11',
 center: [6.09, 1.14],
 zoom: 6,
 });

 map.addControl(new mapboxgl.NavigationControl());*/

 /*L.mapbox.accessToken = 'pk.eyJ1Ijoib2ZmLWluaSIsImEiOiJjazdpMzdwenowZm9sM2ZvM3Y4NTJvbzIxIn0.eVWPpO-Cud_gJDpUetzB7Q';
 var map = L.mapbox.map('map')
   .setView([6.09, 1.14], 10)
   .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
 map.scrollWheelZoom.disable();*/
 L.mapbox.accessToken = 'pk.eyJ1Ijoib2ZmLWluaSIsImEiOiJjazdpMzdwenowZm9sM2ZvM3Y4NTJvbzIxIn0.eVWPpO-Cud_gJDpUetzB7Q';

 var hospitals = {
    "type": "FeatureCollection",
    "features": [
      { "type": "Feature", "properties": { "Name": "Hopital De Bè", "Address": "46XX+6R Lomé" }, "geometry": { "type": "Point", "coordinates": [ 1.2481619,6.1479593 ] }}
    ]
  };


  // Add marker color, symbol, and size to hospital GeoJSON
  for (var i = 0; i < hospitals.features.length; i++) {
    hospitals.features[i].properties['marker-color'] = '#DC143C';
    hospitals.features[i].properties['marker-symbol'] = 'hospital';
    hospitals.features[i].properties['marker-size'] = 'small';
  }

  var map = L.mapbox.map('map')
    .setView([6.119113120243941,1.2076377868652342], 15)
    .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
  //map.scrollWheelZoom.disable();

  var hospitalLayer = L.mapbox.featureLayer(hospitals)
    .addTo(map);

  // When map loads, zoom to libraryLayer features
  //map.fitBounds(libraryLayer.getBounds());
