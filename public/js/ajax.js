
$(document).ready(function (){
    $('#comment').on({
        click : function (e) {

            e.preventDefault()
            let id_article = $('#id_article').val();
            let id_user = $('#id_user').val();
            let content = $('#content').val();
            let _token = $('#_token').val();

            $.ajax({
                url : '/commentaires',
                data: "id_user="+id_user+"&id_article="+id_article+"&contenu="+content+"&_token="+_token,
                type: 'get',
                dataType: 'html',
                processData: false,
                contentType: false,
                success: function (code, status)
                {
                    $('#content').val("");
                    $('#com-art').html(code);
                },
                error: function (code, status, error)
                {
                    alert('Error');
                }
            });

        },
    });
});
