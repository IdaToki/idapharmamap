$(document).ready(function (){

    /*var hospitals = {
       "type": "FeatureCollection",
       "features": [
         { "type": "Feature", "properties": { "Name": "Pharmacie pour tous", "Address": "Avenue de Calais, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.213159,6.1355957 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie Emmanuel", "Address": "Avenue de Duisburg, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2049911,6.1201259 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie d'ADJOLOLOl", "Address": "Rue de la Charité, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.201075,6.1262755 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie Horizon", "Address": "Boulevard du 13 Janvier, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2106287,6.1289734 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie Kodjoviakopé", "Address": "Avenue de Duisburg, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2093005,6.1216541 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie DELALI", "Address": "Boulevard d'Adjidoadin, Agouenyive, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.1952131,6.2078133 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie du Centre", "Address": "Rue de la Gare, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2218713,6.1284905 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie Hanoukopé", "Address": "Rue Detigome, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2189602,6.1370338 ] }},
         { "type": "Feature", "properties": { "Name": "Pharmacie du Boulevard", "Address": "Boulevard du 13 Janvier, Lomé, Togo" }, "geometry": { "type": "Point", "coordinates": [ 1.2280208,6.1361355] }},
       ]
     };*/

     $.ajax({
        url : '/api/pharmacies',
        data: "",
        type: 'get',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (results, status)
        {
            L.mapbox.accessToken = 'pk.eyJ1Ijoib2ZmLWluaSIsImEiOiJjazdpMzdwenowZm9sM2ZvM3Y4NTJvbzIxIn0.eVWPpO-Cud_gJDpUetzB7Q';

            var hospitals = {
                "type": "FeatureCollection",
                "features": []
              };

            for(var i = 0; i < results.length; i++)
            {
                hospitals.features.push({"id":results[i].id,  "type": "Feature", "garde":results[i].garde, "properties": { "Name": results[i].name, "Address": results[i].adress }, "geometry": { "type": "Point", "coordinates": [ results[i].logitude,results[i].latitude ] }})
            }

            // Add marker color, symbol, and size to hospital GeoJSON
            for (var i = 0; i < hospitals.features.length ; i++) {
                hospitals.features[i].properties['marker-color'] = '#ff0000';
                hospitals.features[i].properties['marker-symbol'] = 'hospital';
                hospitals.features[i].properties['marker-size'] = 'small';
            }

            var map = L.mapbox.map('map')
                .setView([6.1284905,1.2218713], 15)
                .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
            //map.scrollWheelZoom.disable();

            var hospitalLayer = L.mapbox.featureLayer(hospitals)
                .addTo(map);

            // When map loads, zoom to libraryLayer features
            //map.fitBounds(libraryLayer.getBounds());

            // Bind a popup to each feature in hospitalLayer and libraryLayer
            hospitalLayer.eachLayer(function(layer) {

            layer.bindPopup(
                '<strong>' + layer.feature.properties.Name + '</strong>' +
                ' <br><p>'+layer.feature.properties.Address+'</p>'+
                '<p><a class="btn btn-info text-white" href="/produit/pharmacie/'+layer.feature.id+'">Acheter des médicaments</a></p>'
            , { closeButton: true });

            }).addTo(map);

            // Open popups on hover

            hospitalLayer.on('mouseover', function(e) {
            e.layer.openPopup();
            });

                },
                error: function (code, status, error)
                {
                    alert('Error');
                }
            });

});

