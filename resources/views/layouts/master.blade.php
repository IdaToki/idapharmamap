<!doctype html>
<html>

<head>
   @include('./layouts/header')
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->

    @yield('content')

    @include('./layouts/script')
</body>

</html>
